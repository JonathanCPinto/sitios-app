import React, {Component} from 'react';
import {Provider} from 'react-redux'
import { persistor, store } from './store';
import { PersistGate } from 'redux-persist/lib/integration/react';

// import MapView, {Marker} from 'react-native-maps';
import EStyleSheet from 'react-native-extended-stylesheet'
import AppNavigation from './src/app-navigator'

type Props = {};
export default class App extends Component<Props> {

  constructor(props){
    super(props);
    global.store = store;
  }

  render() {
    return(
      <Provider
            store={global.store}
            >
          <PersistGate loading={null} persistor={persistor}>
            <AppNavigation />
          </PersistGate>
      </Provider>
    );
  }
}

EStyleSheet.build({
  $bluePrimary: '#282746',
  $blueSecondary: '#2b2a4b',
  $greenPrimary: '#b7cf40',
  $greenSecondary: '#8bc152',
	$textColorPrimary: '#333',
	$backgroundPrimary: '#fff',
	$redPrimary: '#e74c3c',
  $fontSizeTitle: 27, 
	$fontSize: 22,
});
