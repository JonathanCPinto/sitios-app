const BASE_API = 'http://cat.sainetapps5.com'
const token = 'kqOUDG17ICFtYGzXrydDlm9NFDaKaW2AG-6RmVdbftI'

class Api {

    /**
     * [login inicia sesión]
     * @param  {Object}  [datos={}] [description]
     * @return {Promise}            [description]
     */
    async login(datos = {}){
      const query = await fetch(`${BASE_API}/app_login`, {
                method: 'POST',
		    				headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
		    					'Authorization': 'Basic cmVzdF9hcGk6MTIzNDU2Nzg5',
		    					'x-CSRF-Token': token
		    				},
                body: JSON.stringify(datos)
        });
        const data = await query.json();
        return data;
    }

    /**
     * [registrarUsuario regitrar usuario]
     * @return {Promise} [description]
     */
    async registrarUsuario(datos = {}){
        const query = await fetch(`${BASE_API}/user`, {
                method: 'POST',
		    				headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
		    					'Authorization': 'Basic cmVzdF9hcGk6MTIzNDU2Nzg5',
		    					'x-CSRF-Token': token
		    				},
                body: JSON.stringify(datos)
        });
        const data = await query.json();
        return data;
    }

    /**
     * [registrarSitio registra sitio en base de datos]
     * @param  {Object}  [datos={}] [description]
     * @return {Promise}            [description]
     */
    async registrarSitio(datos = {}){
        const query = await fetch(`${BASE_API}/node`, {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': 'Basic cmVzdF9hcGk6MTIzNDU2Nzg5',
                  'x-CSRF-Token': token
                },
                body: JSON.stringify(datos)
        });
        const data = await query.json();
        return data;
    }

    /**
     * [getSitios obtiene sitios]
     * @return {Promise} [description]
     */
    async getSitios(idUsuario){
        const query = await fetch(`${BASE_API}/node?type=place&author=${idUsuario}`, {
                method: 'GET',
		    				headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
		    					'Authorization': 'Basic cmVzdF9hcGk6MTIzNDU2Nzg5',
		    				},
        });
        const data = await query.json();
        return data;
    }


}

export default new Api();
