'use strict'
import React, { Component } from 'react';
import {
		 AsyncStorage,
		} from 'react-native';

class MiSesion extends Component{
	constructor(props){
		super(props);
	}

	/**
   * [setSesion configura la sesión]
   * @param {[type]}   datos    [description]
   * @param {Function} callback [description]
   */
	setSesion = (datos, callback) => {
		AsyncStorage.setItem('user', JSON.stringify(datos))
			.then((user)=>{
					if(callback)
						callback();
        });
	}

	/**
   * [migetSesion obtiene la sesión]
   * @param  {Function} callback [description]
   * @return {[type]}            [description]
   */
	migetSesion = (callback)=>{
		AsyncStorage.getItem("user")
	   	.then((user)=>{
		   		if(callback)
		   			{
		   			 callback(token, user);
		   			}
	   	});
	}

  /**
   * [cerrarSesion cierra la sesión]
   * @param  {Function} callback [description]
   * @return {[type]}            [description]
   */
	cerrarSesion = (callback) => {
		  	 AsyncStorage.removeItem("user")
		  	 .then((user)=>{
		  	 	if(callback)
		  	 		{
		  	 		   callback();
		  	 		}
		  	 });
	}
}

export default new MiSesion();
