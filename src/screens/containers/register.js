import React, { Component } from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet'
import API from '../../../utils/api'

class Register extends Component {

  constructor(props){
		super(props);
		this.state = {
			names: 'Jónathan Cuellar',
			mail: 'jonathancuellarp@gmail.com',
			pass: '12345',
		}
  }

  async handleRegister(datos){
    const response = await API.registrarUsuario(datos);
    if(response.id && response.id.uid != 0){
        alert(response.id.message);
        this.props.navigation.navigate('Login');
    }else{
        alert(response.id.message);
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View>
          <TextInput
            style={styles.input}
            placeholder="Nombres"
            placeholderTextColor="white"
            value={this.state.names}
            onChangeText={(names) => this.setState({names})}
          />
          <TextInput
            style={styles.input}
            placeholder="Correo"
            placeholderTextColor="white"
            value={this.state.mail}
            onChangeText={(mail) => this.setState({mail})}
          />
          <TextInput
            style={styles.input}
            placeholder="Contraseña"
            placeholderTextColor="white"
            secureTextEntry={true}
            value={this.state.pass}
            onChangeText={(pass) => this.setState({pass})}
          />
          <TouchableOpacity
            onPress={()=>{
                this.handleRegister(this.state)
            }}
            style={styles.button}
          >
            <Text style={styles.buttonLabel}>Registrar</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const {width, height} = Dimensions.get('window');

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
    marginBottom: 10,
  },
  input: {
    marginBottom: 10,
    width: width-50,
    height: 50,
    paddingHorizontal: 10,
    borderRadius: 5,
    backgroundColor: '$bluePrimary',
    color: 'white',
  },
  button: {
    backgroundColor: '#99c84a',
    borderRadius: 5,
  },
  buttonLabel: {
    color: 'white',
    padding: 10,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center'
  }
})

export default Register
// export default connect(null)(Register)
