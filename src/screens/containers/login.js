import React, { Component } from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Image,
  TouchableOpacity,
  Dimensions,
  ActivityIndicator,
  Keyboard
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet'
import { connect } from 'react-redux';

import API from '../../../utils/api'
import SESSION from '../../../utils/session'

class Login extends Component {

  static navigationOptions = {
      header: null
    };

  state = {
    email: 'jonathancuellarp@gmail.com',
    password: '12345',
    isLoading: false
  }

  async handleLogin(){
    const response = await API.login(this.state);
    // inició bien
    if(response.uid != 0){
      console.log('inicio sesion');
      this.props.dispatch({
      type: 'SET_USER',
      payload: {
        user: response
        }
      })
      SESSION.setSesion(response);
      this.props.navigation.navigate('Loading');
    }
    else{
      console.log(response);
      alert(response.message);
    }
    this.setState({
      isLoading: false
    });
  }
  render() {
    return (
      <View style={styles.container}>
        <View>
          <View style={styles.containerTitle}>
            <Text style={styles.textTitle}>Mis Sitios</Text>
          </View>
          <TextInput
            style={styles.input}
            placeholder="usuario"
            placeholderTextColor="white"
            value={this.state.email}
            editable={!this.state.isLoading}
            onChangeText={(email) => this.setState({email})}
          />
          <TextInput
            style={styles.input}
            placeholder="contraseña"
            placeholderTextColor="white"
            secureTextEntry={true}
            value={this.state.password}
            editable={!this.state.isLoading}
            onChangeText={(password) => this.setState({password})}
          />
          <TouchableOpacity
            onPress={()=>{
                Keyboard.dismiss();
                // valida que los campos esten completos
                if(this.state.email == '' || this.state.password == ''){
                  alert("Por favor completa los campos");
                }
                else{
                    this.setState({
                      isLoading: true
                    });
                    this.handleLogin()
                }

            }}
            style={styles.button}
          >
            <Text style={styles.buttonLabel}>Entrar</Text>
          </TouchableOpacity>
        </View>
        {
            this.state.isLoading && <ActivityIndicator size='large' color='#57924E'/>
        }
        <View style={styles.containerRegister}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('Register');
            }}
          >
          <Text style={styles.textRegister}>¡Crea una cuenta!</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const {width, height} = Dimensions.get('window');

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
    marginBottom: 10,
  },
  input: {
    marginBottom: 10,
    width: width-50,
    height: 50,
    paddingHorizontal: 10,
    borderRadius: 5,
    backgroundColor: '$bluePrimary',
    color: 'white',
  },
  button: {
    backgroundColor: '#99c84a',
    borderRadius: 5,
  },
  buttonLabel: {
    color: 'white',
    padding: 10,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  containerRegister:{
    paddingTop: 10,
    paddingBottom:2,
    borderBottomWidth: 2,
    borderBottomColor: '#99c84a',
  },
  textRegister:{
    fontSize: 18
  },
  containerTitle:{
    paddingHorizontal: 3,
    paddingVertical: 5,
    alignItems: 'center',
    justifyContent: 'center'
  },
  textTitle:{
    fontSize: '$fontSizeTitle',
    fontWeight: 'bold',
    color: '$blueSecondary'
  }
})

export default connect(null)(Login)
