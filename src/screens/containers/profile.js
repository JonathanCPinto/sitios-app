import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet'
import SESSION from '../../../utils/session'
import {connect} from 'react-redux'

function mapStateToProps(state){
    return{
      user: state.user
    }
}

class Profile extends Component {

  render() {
    return (
      <View style={styles.container}>
        <View>
          <View style={{paddingBottom:10}}>
            <Text style={styles.text}>Nombre: {this.props.user.user.name}</Text>
            <Text style={styles.text}>Email: {this.props.user.user.mail}</Text>
            <Text style={styles.text}>UID: {this.props.user.user.uid}</Text>
          </View>
          <TouchableOpacity
            onPress={()=>{
                SESSION.cerrarSesion();
                this.props.navigation.navigate('Loading');
            }}
            style={styles.button}
          >
            <Text style={styles.buttonLabel}>Cerrar Sesión</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const {width, height} = Dimensions.get('window');

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    backgroundColor: '#99c84a',
    borderRadius: 5,
  },
  buttonLabel: {
    color: 'white',
    padding: 10,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  text:{
    fontSize: 20,
    fontWeight: 'bold'
  }
})

export default connect(mapStateToProps)(Profile)
