import React, {Component} from 'react'
import {
  AsyncStorage
} from 'react-native'

import LoadingLayout from '../../sections/components/loading'
import {connect} from 'react-redux'

class Loading extends Component{

    componentDidMount(){
      console.log('Loading');
      AsyncStorage.getItem("user")
  	   	.then((user)=>{
            if(JSON.parse(user))
                this.props.navigation.navigate('App');
            else
                this.props.navigation.navigate('Login');
  	   	});
    }

    render(){
      return <LoadingLayout />
    }
}

function mapStateToProps(state){
    return{
      user: state.user
    }
}

export default connect(mapStateToProps)(Loading);
