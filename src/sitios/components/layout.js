import React, {Component} from 'react'
import{
  View,
  Text
} from 'react-native'

class Layout {
  constructor() {

  }

  render(){
    return(
        <View>
          {this.props.children}
        </View>
    )
  }
}

export default Layout;
