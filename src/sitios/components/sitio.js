import React from 'react'
import {
  View,
  Text,
  Image
} from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'

function Sitio(props){
        return(
            <View style={styles.containerItem}>
              <View style={styles.containerImage}>
                <Image
                  style={styles.image}
                  source={{uri:props.item.image}}
                  />
              </View>
              <View style={styles.containerBody}>
                <Text style={styles.text}>{props.item.title}</Text>
              </View>
            </View>
        )
}

const styles = EStyleSheet.create({
  containerItem:{
    flexDirection: 'row',
    backgroundColor: '$backgroundPrimary',
  },
  containerImage:{
    flex: 2,
    paddingLeft: 5,
  },
  containerBody:{
    flex: 4,
    justifyContent: 'center',
    backgroundColor: '$backgroundPrimary'
  },
  image:{
    width:70,
    height:90,
    resizeMode: 'contain',
  },
  text:{
    fontSize: 20,
    fontWeight: 'bold'
  }
});

export default Sitio;
