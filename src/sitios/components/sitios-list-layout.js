import React, {Component} from 'react'
import {
    Text,
    View
} from 'react-native'

function SitiosListLayout(props){
        return(
            <View>
              <Text>{props.title}</Text>
              {props.children}
            </View>
        )
}

export default SitiosListLayout;
