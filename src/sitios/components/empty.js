import React from 'react'
import {
  View,
  Text
} from 'react-native'

function Empty(props){
        return(
              <View>
                <Text>{props.texto}</Text>
              </View>
        )
}

export default Empty;
