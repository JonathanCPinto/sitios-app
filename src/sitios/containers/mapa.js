import React, {Component} from 'react'
import{
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions
} from 'react-native'
import {NavigationActions} from 'react-navigation'
import MapView, {Marker} from 'react-native-maps';
import {connect} from 'react-redux'

class Mapa extends Component{
  render(){
    let markers = null;
    if(this.props.list.sitiosList)
      markers = this.props.list.sitiosList.list.map((value, key) => {
          return <Marker
                    key={key}
                    coordinate={{
                      latitude: parseFloat(value.lat),
                      // latitude: 4.15049,
                      longitude: parseFloat(value.lon),
                      // longitude: -73.6325,
                    }}
                    title={value.title}
                    description={'Descripcion'}
                    // image={require('../assets/pin.png')}
                  />
    });
    return(
        <View style={styles.container}>
          <MapView
                   style={styles.map}
                   region={{
                     latitude: 4.1518,
                     longitude: -73.638,
                     latitudeDelta: 0.015,
                     longitudeDelta: 0.0121,
                   }}
                   // annotations={markers}
                 >
              {markers}
              {/*<Marker
                coordinate={{
                  latitude: 4.1518,
                  longitude: -73.638,
                }}
                title={'Unicentro'}
                description={'Centro Comercial'}
                // image={require('../assets/pin.png')}
              />*/}
              </MapView>
              <TouchableOpacity
                  onPress={() => {
                    this.props.dispatch(
                      NavigationActions.navigate({
                        routeName: 'Loading'
                      })
                    );
                  }}
                >
                <Text>Click</Text>
              </TouchableOpacity>
        </View>
    )
  }
}

function mapStateToProps(state){
    return {
        list: state.sitiosList,
    }
}

const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
 container: {
   // flex: 1,
   ...StyleSheet.absoluteFillObject,
   height: height,
   width: width,
   justifyContent: 'flex-end',
   alignItems: 'center',
 },
 map: {
   ...StyleSheet.absoluteFillObject,
 },
});

export default connect(mapStateToProps)(Mapa);
