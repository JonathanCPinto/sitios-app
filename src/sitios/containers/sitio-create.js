import React, {Component} from 'react'
import {
    View,
    Text,
    TouchableOpacity,
    Dimensions,
    TextInput,
    Image,
    AsyncStorage
} from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import {connect} from 'react-redux'
import API from '../../../utils/api'

function mapStateToProps(state){
    return{
      user: state.user
    }
}

class SitiosCreate extends Component {

      state = {
        type: 'place',
        title: '',
        field_image: [
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAw1BMVEX////y8vIAAACzmnz19fX5+fmggVz7+/uqknbx8fHS0tLt7e2YelciHhguLi63nX9AQEB4Z1PDw8PY19qslHeEakylhV9jY2PJyclqW0mCgoSdf1rh4eFLQTSSfmVyYk+xsbGJdl+eiG5YTD1rVj4yKR0MDAyoqKgxMTFsbGwWExAkJCQYGBhFRUU5OTmBb1pPT0+RkZEvJhuojGp8fHwrIxl1XkNbSTSAZ0pAMyVxW0FVRTFWSjuenp47LyJiVURZWVlKqjh0AAARmElEQVR4nNWdeUPbPA/AkzYhbTm6QegYlHFswGDj2vEw9g627/+p3iaWfCS2IydKxvTX0mbCv0qxfMhKFNklA3F8HXqfuGl9JGSdRx2hdZ6vUiFNGuC2xhaJu5oIw9QR7nP/BikvIN7WQJgx/1mPmVOaCmqL5G1+wlB1za1zIqaJkCYVcFtji6Q6L2G4On/ryh/BTpj0BeglRHUJXV1D6wSi5TsiYEpska7OQ0gFxNsaftdSnd2TsyQupREwhvuaWqSrcxNmYepiCmCBaPsKVPACgjonYai6JsCVJpehewV0EvIDlojDA7oI+wAsEC3fxbyANXV2wkDAKQ3Qqo4ZsO4QVsJ+LDgkoNYiG+GAgMzPoO33shD2BBhbvqICTtsDWgip6ga34DT8GSykRvjvuqijRVXCvlzUQpEkvQBW1VUIh3sGi1H0ABasEvYUBx2AyRCAJuGALpoCYe+ABuGAgBkQ9hkmQDTCnsKEzUWL2T5lwhvYIqs6RThgmEBC5jhob5Ek5J7weiyIhD0O1TSRhEMCCsKe4yAKEi6TAQFLwl6HapqYhEM8g+UNVECGFhmEA4QJuKNxSZ7JRSOTcCAXjbzbM6WwhAmQqSIcDrBJWAPXMRBeNG6qvFxAX8u3P45QPm7T1A0AyDBUA9neGumy5WP8Fy14/GpUlVfHTeq6holm4RmqrWT7psZXyI3dji/PRRsteGnlK+TSo44J0PPg8LnoG8Nw+sUbtzouQHezGMOERnh3sXN85yMMfQYbd4LduQp8gIrw1cVsZyXH105CZgt6xtxM0yUhQHhzvFMC7uwssWutEvIDugg5h2qScB8BV4j7VkLmMJE69rkj9lUiIPy2o8RKyG1B104+s4tGyobLpdeGzIDuXAXOoVrZogQI7/aV3NUJuQFjJyGzi6ZxYsRDQzRCtgmvVFcSDgFII+QHLAkHASQRcrvoVBAOA0ghZNh8qbduMEACIfNQDfOwLOp6AYyTPw2E7L2oUx1zHMSok+7fnQugzTMhm+Ly/G6/oUW6BD2D3QBb7IjDCs3PhZCf4vKjoW4wC3af0RvqxO8FazQfFmuFLD6Iy60AdYGAtj6L20UNdW7Cl/cMtkvacBK+XBcN3OpwETJPlzzqZGZwexW61BxCEgqRhIEWpMVBOyDrhl7d44Hw9XxcyPw1EGa8LuoOExlkm1A7mWBAP+EAFoRsk94s6CccAlAQ9gjoI+w/DkYiF4N7qGaqcxP2HyZkPg1RRbsdcSdh/2FCEnZRocQ1bnARDuCiQNh0dqlr+mmF8Egbl3qEIUyUagjn4Trn15qEOY2Q4xks9QwAaBJOaISBM3r3A+TZnaGqEOIbu+uEOY2Qy4KEfBr8S13yijTCyYREGPgM+tawqBbslDilCCc0QubkRQYVDdNLSZjTCPlctFGYTikgYQnYTMgTB0lCfQab9nKA8IhG+O9ZsEo4fu0l/OeewUIMwnzuJWSYLlGF8aTQJ+05zOW49JPtVqahmtLnUcEHqBPmq3HprZuQ+xn0hHs+F13JZ0mYjxXhZ0uDUB2Ti3oSoFkBI9i3uM3zsUZ4XruP2YK+HHbi007d8kfCsZg9nTgImZ/BzJ1twn0cMQPCk7mXkNuC7mwTZheNkgQI3wHhOysh80gmc2eb8B/qxgz2CuHI+AP8FnQSMrtoEidLIDoFwlO4nuot4gZ0Zpuwu+jqlh0gmoieZjyB66XWIqLjBOQ0OIqb9AAYJzMgyoEwh+sdo0UBgBy1PxgB42TPQTgzWkRxUWKYiIHQ8l3cA2CcbAPRGAWu94wW8Vowthc3Ye9kDMIrSXglPtg2WsTWyXha1w9gHAHhpiSEdJNjo0VccZABMPiQ8oUAepaEz+KDC71F3Ba0FVbox4KrFkG+7OEcAOeH4oP9yP+TO9T5hALI2skIdXCk5L0kfC8+uOzNRa2lMXqzYBRBYttvSfhbfPCnnzAxPGAE5yteS0JYxriOqIBhLjo4YAILUSeSEKZPW+vuh8atzine0hhJb8/gStbPjYG3Gnqfw7H1MHUu8QMmPQJGcmohowVOLmYJ/Rns0slg9ZaeACOYWpxPJOHkXCMcwEWBsC9AOaTJJWGOg5p0CEBq9ZbWgNE9DGnmknAOg5r7dIA42FP1Fr1F3ypDGjWouWzcveweB8Oqt7QbW0HAf6sIxxDy79qoszD4AHmrt1hblH2qhMNxnuO6vv/vsgD2Ur3FbFECoeEdEuaTHEL+yKuRw0WjkOotbYf/1XBYbEDheuLSrsmnrgbQADhA9RaohnGuAU4wIB6Hq6u2vwmwOZ+m80GaJwiHcw0QZ/lP4eoqzW8EZK/eUm/RpR4sBOBkjOHCqY7nGSQIw1EocMijuQKcjCFZob7B1qhOl5cBGI1UsEDAyRg7U6+6LiMZonAA4oL3RAHmamF/Zvkf/5aLqsJCcx1wPB+5O9N/y0XlQtuXuXLRgvCL+Hg/VJ2UFwOIizTvx5oF1XLbdag6FCogY/UWV4tgVAr5UEU6TUkInWkt44T5GRygegvuHb4rB2unpWhLNdoOG0kdNpwKOED1FuxoTleASFU8ibjDdhymDhpOBuxcvaW5RTD9fZ5UCHHv4ptFHVccZKze4mkRdDS/8gqhrathtuAw1VuwVOJtkdI20QlhEqwVaGcH7Fy9hdIiHNEUHc3YIESDzirq2FzUU72FERDX2TbLQK8TyhXFe1THGybSoaq3QMWI53ER6A3C+aO4aFc5grD4OVD1FmB6K0YypzIeqpg/ClEXADhQ9RZ8DE9gnVSI+Deu1czo6kIAHfk0zLXu8TFUeSaa4PTiiWXzxWjdVBB2B2z8yTOoy/Y4txHCg/iK24KY5DIAYIzR8AgItemT9iBOe3BRuzp2QJkMBXujRl+qIuJ2wuqiAwLG6b75GJqEMvlrP+V0Ub7yNIQWpfAY4q5ThRB3oF6l7M/gQIAyr/S1gxBSMkY7CbMFbRVM+ngdQ/pU8cqql+L1U9NBXYZOppfcqxQ2Ds/GDsLxmbj+0zSR7e6izNVbIIMVN53k1miNEDZKR/697lBACwVz9RYsyoILGO8chLncRvQWMA91UTtgwg8YZ7AloxJnTcJi1g+ptM4NmgDAxL0uwVy9JYvxFZnA82uOIgmL0Xe+EtyCcmxfhAD6LMhbvWXVouX2SvZmF/gYvkNBpzwp/l0KPogXs0L23K9g7fJCOGL1loCRDKKFy3FbQI+LhlZvoYxk+AgZXDTird4iAj0bIc9BmqDqLbRAz0VItqB/dTekegsxDjIRsrgoELICqkjfjZCjk0FCXsA4YiFkctHIu8HWErDQmaVwfPt57WAlaw+AsIymU1x/exBfwQ7N59R4lz1HHERV/ICFIMZ/ZTHIBaxvXxfqcHlqU3z1H9w5s6prP6OnSktA3FQbbZTVLjfg6qKMl0/W777Z1DHla/UAiGPS54VezhNeR4azKlHscwFuOrKoI8bBvwGIZypNJ70RJ0iiG6ub7tXUde1FewSUTioqsu7C1ZPwKjzLNtoVFVsrbvpyXVTLw8W0YLHINn+rO+lKHS4Ui8k/LrlBwjDDjL4nQH0lAnvSW4EAyzF38lQHvPzhTHyNu8GzEEDqRjyheksLQOmkubG2XTipGB2jm4q1cEzL+BYASO1kuldvsS7SpVtWJ91JUJ25RoVuupWyFxjqXr3Ffn7QdFJcM7xOlDp003HFTZk7GU8OeydATNaDg07KSdUETrqpWKGCJNv9lAZIdVFC9ZZ2gBjua06q1NnddEQEpJan6Vy9xbVQXulJMdwb9VhxbGr2pmKfjemsl6d6S9ctWWNPTfWkxj2mm+I+27eMbsEGF6VUb2npoi4nNVcK7UF/xHaGjlC9pf07X3BMCj0pOOmryl3XDjflGckUGzEdq7d4ApcR7nPMKbmo3OUO+j6hApb3dKre4gHM9HCf53YnrbppjkHfb0Kqi8ZA2L6wgq9QgN6T5pMJZKz/qd2nby5OcmNs6hJqJwN39QQow30+n+e500kNN13dJ4O+5896dnhtrWMArP6Ue8WODL739+zk5HYlv5BQfKlkDwnfl/edgK23xJceQI7SGG0tqN7Z3FU+Wv5smIv2AhjV32rcVqqhJQrsZHpx0Z4JGRKIGbLneiQkzuh7ddFeCQNdtC/A/ggZOhli9ZaGvCIgvNpsL1c2QgYXJVZvaSp5BIRHuXFKzSnGaT0hmHJqEBLftsVQvaWxplOFsDxIqWd2V65yITq0jZDBRTNa9ZZGwAph2fRDIZidL65ExnB+Kq4e/YQMnQzm03QGNAlzfVIkpkhzGML9Kq5UXQUvIXHRiVC9hQHQIBTOVyGEw1zvy9dc5BRCBhdFQgZArC3/NpedjJNw4ifEevQMLoqEHSYkUnD14iifVAuVVgknLkLMGhaTi5AZvROQVL2FZEG5enGqwoSd8FfuIpTZi+VqBkMcREIWQCz4fKgFAHtPcyg6WxuhXHSbsnQySEgEbPhLmGNyq7f4bSm/4YDFibi6FTacwJcGIaYvHg9YvYVmQbnZcmWGcD3Ej4t/FEEeoon5JVgdxm13Ee13JWxWNeXTUAFxJ+K92WLV9K/ljvbBhpCvX+23YVnMciuOwUWbhQqY4VL+qYUQ6DTAjYPVxVfLwFVuArAepCGoaC6dDzlQmzVAhVcBXLNC4kbO55CBJIMFm9ba5RmuowphvljzABaJGhVGeaJtuzFveUAXTeIM3xBfaa/OpwDXDFlUfhPQ9CZ7SYDJEhZ0Dw0TfjVA1jY2dnd3f+zurmy4MNCNXgdD4nk9rb0vQMJ2kEyaPdEIcx3vYPfD4yb8DKPzzccPuwc6pG56XOA/ZgH0fE234OpHsARDZcDF4scjwik5f/yhWVKaMZfHTP6wAHriJBVwqh3D+z2vAy42Hup4APmwsaggFqUXYPjqK8hHBnSPZ0IsGKd4ZFsFQ+WeDw48IQ/KWQFQDcnvnX+VDugkDDutl0GTNuuAP6+8gCvH/qkhwvoHhETnWaEAQBdhiItqb3eQBbulg35p4Cvki3RVXKPDkGjdggpIfXNnmwS5aFwPhgi4W8f5/Olz/UPIwtzYAEIMifYjbXRAJ2HogVLcsMZgCCaRmcHA9uZ+tlyuJ+vL5ez+jclZZgsXYwGolaXNErsAughDs+fkzBCCIfSiC6OLebNtBvB071L/+mGxJqYcYlqlZontAd3VW8KewWJ0bAbD3AJ4v27J+5A9cIkoCDdgFVLOEtsDOvNpAp/BFSDODCEY1l30cpnZW7Su2fGDGLUeiJEbzhKrITEkQ9pRvaVFgieaQhz3FT4q09NXsp26M5321G0/S0QR+eeOkBiUAm5fIw20YLklCznd3+eaj27Iht8sU1+L1tWWHMw6RF7xd/GZWZIvLMe9G6CW/4hmEMFQ9KOLM+mh5ZaIr0XSU88OymljOZmqLJwGAorWMVhQTMGxhRNlQvUQ/iGkU2I0hRMmwoi4cKqFxNBTCkwuGqXQlEPNhNJHr+MmCxYarqWfrikjYkiU7WQ4htHKgmYwBBP+Dz77tE4BTNbhSR79TxmxFhL/GiBmUY6KldCqCbe9nYxsUYrjWmnEYtkc3YAdMPCdL0vz5zdMuE/N+JVLkTYtyyBAT2EFN7sPUAbDXdk0ZUKKi0K3ZxqxFBy234cAeiwYVtxEpiZhMukX1TI5XHtqfgEJtkgmKj5oizeYy5fxALas/YHB8INq2QGMKc+n5JdKpPJts1dqJVWGnD35Z7sBUkpjlMUXjNRbDIZqvWXxA02YNaxYw58V6vBc6Q+lCL39MkqglEh7QGx55fOZEPlxGs/2SplJgUZ831WCR0KPtdussodSXGB3+qxp+o5GrPxVrfRwAh/5AaH2Rw0Q3+KLiclpMsXA9XdFe7MJ/MTnifQvm6XLqgqWr/BlYjMETKY3fw9Lk5sa4VaS+QCzErH+lUlY/AbxiyWcpl7AzF45wiAsf4SXS7iepnYzSUTr4RmNUJg5ebGE09RT2TpyAOqEAjB7wYT+lBLH7oUizEDSF0zoMFMU/R+IOmvMtPZ0mQAAAABJRU5ErkJggg=='
        ],
        uid: '',
        lat: '4.13903',
        lon: '-73.6378'
      }

      async handleCrearSitio(data){
          const response = await API.registrarSitio(data);
          if(response.id){
            alert('El sitio ha sido registrado');
            this.props.navigation.navigate('Sitios');
          }else{
            alert('Error guardando el sitio');
          }
      }

      render(){
          return(
                <View style={styles.container}>
                  <Image style={styles.img} source={{uri: this.state.field_image[0]}}/>
                  <TextInput
                    style={styles.input}
                    placeholder="Nombre del sitio"
                    placeholderTextColor="white"
                    value={this.state.title}
                    onChangeText={(title) => this.setState({title})}
                  />
                  <TextInput
                    style={styles.input}
                    placeholder="Latitud"
                    placeholderTextColor="white"
                    value={this.state.lat}
                    onChangeText={(lat) => this.setState({lat})}
                  />
                  <TextInput
                    style={styles.input}
                    placeholder="Longitud"
                    placeholderTextColor="white"
                    value={this.state.lon}
                    onChangeText={(lon) => this.setState({lon})}
                  />
                  <TouchableOpacity
                    onPress={() => {
                        data = {
                            type: this.state.type,
                            title: this.state.title,
                            field_image: this.state.field_image,
                            uid: this.props.user.user.uid,
                            lat: this.state.lat,
                            lon: this.state.lon
                        }
                        this.handleCrearSitio(data);
                    }}
                    style={styles.button}
                  >
                    <Text style={styles.buttonLabel}>Guardar</Text>
                  </TouchableOpacity>
                </View>
          )
      }
}

const {width, height} = Dimensions.get('window');

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    backgroundColor: '#99c84a',
    borderRadius: 5,
  },
  buttonLabel: {
    color: 'white',
    padding: 10,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  input: {
    marginBottom: 10,
    width: width-50,
    height: 50,
    paddingHorizontal: 10,
    borderRadius: 5,
    backgroundColor: '$bluePrimary',
    color: 'white',
  },
  img:{
    width: 150,
    height: 200,
    resizeMode: 'contain',
  }
});

export default connect(mapStateToProps)(SitiosCreate);
