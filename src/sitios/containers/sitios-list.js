import React, {Component} from 'react'
import {
    View,
    FlatList,
    Text,
    TouchableOpacity,
    ActivityIndicator,
    Image,
    ScrollView
} from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import Layout from '../components/sitios-list-layout'
import Sitio from '../components/sitio'
import Empty from '../components/empty'
import {connect} from 'react-redux'
import API from '../../../utils/api'

function mapStateToProps(state){
    return {
        list: state.sitiosList,
        user: state.user
    }
}

class SitiosList extends Component {

    static navigationOptions = {
      header: null
    };

      state = {
        isLoading: false,
      }

      renderEmpty = () => <Empty texto='No hay Sitios :o'/>
      keyExtractor = item => item.nid.toString()
      renderItem = (item) => {
        return <Sitio item={item} />
      }


      async componentDidMount(){
       this.setState({
         isLoading: true
       });
       const datos = await API.getSitios(this.props.user.user.uid);
       if(datos){
         global.store.dispatch({
           type: 'SET_SITIOS_LIST',
           payload: {
             sitiosList: datos
           }
         })
         this.setState({
           isLoading: false
          });
       }
     }

      render(){
          // console.log(this.props);
          let list = [];
          if(this.props.list.sitiosList)
                list = this.props.list.sitiosList.list;
          return(

                <View style={{flexDirection:'column', justifyContent:'space-between'}}>
                  <View>
                    <TouchableOpacity
                      onPress={()=>{
                        this.props.navigation.navigate('NuevoSitio');
                      }}
                      style={styles.button}
                      >
                      <Text style={styles.buttonLabel}>Nuevo Sitio</Text>
                    </TouchableOpacity>
                  </View>
                  <ScrollView>
                    <FlatList
                      keyExtractor={this.keyExtractor}
                      data={list}
                      ListEmptyComponent={this.renderEmpty()}
                      // renderItem={({item}) => <Text>{item.title}</Text>}
                      renderItem={({item}) => (this.renderItem(item))}
                      />
                    {
                      this.state.isLoading && <ActivityIndicator size='large' color='#57924E'/>
                    }
                  </ScrollView>
              </View>

          )
      }
}

const styles = EStyleSheet.create({
  button: {
    backgroundColor: '#99c84a',
    borderRadius: 5,
  },
  buttonLabel: {
    color: 'white',
    padding: 10,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  text:{
    fontSize: 20,
    fontWeight: 'bold'
  }
});

export default connect(mapStateToProps)(SitiosList);
