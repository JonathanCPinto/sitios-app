import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';

function Loading(props) {
  return (
    <View style={styles.container}>
      {/*<Image
        source={require('../../../assets/login.jpg')}
        style={styles.logo}
      />*/}
      <Text style={styles.text}>Mis Sitios</Text>
      <ActivityIndicator size='large' color="#fff"/>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#2b2a4b',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 200,
    height: 80,
    resizeMode: 'contain',
    marginBottom: 10,
  },
  text:{
    fontSize:25,
    color: '#fff',
    fontWeight: 'bold'
  }
})

export default Loading;
