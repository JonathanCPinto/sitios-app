import React from 'react'
import {
  createStackNavigator,
  createBottomTabNavigator,
  createSwitchNavigator,
  createDrawerNavigator
} from 'react-navigation'
import EStyleSheet from 'react-native-extended-stylesheet'
import IconTab from './sections/components/icon-tab'

import Login from './screens/containers/login'
import Register from './screens/containers/register'
import Mapa from './sitios/containers/mapa'
import Profile from './screens/containers/profile'
import Loading from './screens/containers/loading'
import Sitios from './sitios/containers/sitios-list'
import NuevoSitio from './sitios/containers/sitio-create'

// Login Navigator
const LoginNavigator = createStackNavigator(
  {
    Login: Login,
    Register: Register,
  },
  {
  }
);

// Sitios Navigator
const SitiosNavigator = createStackNavigator(
  {
    Sitios: Sitios,
    NuevoSitio: NuevoSitio,
  },
  {
  }
);

// bottom tabs
const TabNavigator = createBottomTabNavigator({
      // lista de sitios
      Sitios: {
          screen: SitiosNavigator,
          navigationOptions:{
            title: 'Sitios',
            tabBarIcon: <IconTab icon="📰"/>
        }
      },
      // mapa
      Home: {
          screen: Mapa,
          navigationOptions: {
            title: 'Mapa',
            tabBarIcon: <IconTab icon="🌎"/>
          }
        },
      // vista del perfil
      Profile: {
          screen: Profile,
          navigationOptions:{
            title: 'Profile',
            tabBarIcon: <IconTab icon="😄"/>
          }
      },
      },
      {
        navigationOptions:{
          // header: <Header />
          header: null
        },
        tabBarOptions:{
          labelStyle:{
              // fontSize: EStyleSheet.value('$fontSizeSm')
              fontSize: 17
          },
          activeTintColor: 'white',
          // activeBackgroundColor: '#65a721'
          // activeBackgroundColor: EStyleSheet.value('$bluePrimary')
          activeBackgroundColor: '#282746'
        }
      });

const SwitchNavigator = createSwitchNavigator(
      {
          Loading: Loading,
          App: TabNavigator,
          Login: LoginNavigator,
      },
      {
          initialRouteName: 'Loading'
      }
);

export default SwitchNavigator;
