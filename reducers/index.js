import {combineReducers} from 'redux'

import user from './user'
import sitios from './sitios'

const reducer = combineReducers({
    user: user,
    sitiosList: sitios,
})

export default reducer;
