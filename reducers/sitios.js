function sitios(state = {}, action){
    switch(action.type){
      case 'SET_SITIOS_LIST':{
          return {...state, ...action.payload}
      }
      default:
        return state;
    }
}

export default sitios;
